//Contains all the functions and business logics of our application
const Task = require("../Models/Task.js");

module.exports.getAllTask = () => {

	return Task.find({}).then(result => {
		return result;
	})
};

module.exports.createTasks = (requestBody) => {
	return Task.findOne({name: requestBody.name}).then((result, error) => {
		if(result !== null && result.name == requestBody.name){
			return 'Duplicate Task Found.'
		} else {
			let newTask = new Task({
				name: requestBody.name
			})

			return newTask.save(requestBody).then((savedTask, error) => {
				if(savedErr) {
					console.log(savedTask)
					return 'Task Creation Failed';
				} else {
					return savedTask;
				}
			})
		}
	})
};

module.exports.getTask = (paramsId) => {
	return Task.findById(paramsId).then((myTask, err) => {
		if(err) {
			console.log(err)
			return 'Task was not removed';
		} else {
			return myTask;
		}
	})
}