const express = require("express");

const taskController = require("../myControllers/myTaskController.js");

const router = express.Router();

router.put("/update/:_id", (req, res) => {
	taskController.updateTask(req.params).then(resultFromController => {res.send(resultFromController)});
})

module.exports = router;
